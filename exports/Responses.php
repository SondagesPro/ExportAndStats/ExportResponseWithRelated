<?php

/**
 * Export a single response as array key by em, value
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @copyright 2020-2024 OECD
 * @license AGPL v3
 * @version 0.7.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace ExportResponseWithRelated\exports;

use Yii;
use CDbCriteria;
use PDO;
use Exception;
use Survey;
use CHtmlPurifier;

class Responses
{
    /* @var integer surveyid */
    public $surveyId = 0;
    /* @var string|null language */
    public $language;

    /* @var \ExportResponseWithRelated\AvailableRelatedExport */
    protected $oAvailableRelatedExport = null;
    /* @var string[] */
    protected $aRelatedCode = array();
    /* @var array[] */
    protected $aRelatedExports = array();
    /* @var array[] */
    protected $aRelatedExportsOptions = array();

    /** Selection part **/
    /* @var string token */
    public $token = "";
    /* @var string boolean : get group of token if exist */
    public $tokenGroup = false;
    /* @var integer responseId */
    public $responseId = 0;
    /* @var integer[] responseIds */
    public $responseIds = array();
    /* @var boolean */
    public $completed = true;
    /* @var null|string[] */
    public $aColumns = null;
    /* @var array[] */
    public $aChildsColumns = array();

    /* @var array[] */
    private $aQuestionColumns = array();
    /* @var array[] */
    private $aQuestionAnswers = array();

    /* @var string dataHeader : (code|text|none|both) : way to export header*/
    public $dataHeader = 'code';
    /* @var string answerFormat (full|code|both) */
    public $answerFormat = 'full';
    /* @var boolean
     * @deprecated 0.7.0, use $answerFormat */
    public $answerCode = false;

    /* @var string[] */
    protected $aHeader = array();
    /* @var string[] */
    protected $aDatas = array();

    /* @var null|int[] array of reponseid related to current export , key are survey id, value can be responseIds or true if need to export all*/
    protected $responseIdsByRelated;

    /* @var boolean $disableIdCheck used if origina surey are not exported by id or ids */
    public $disableIdCheck = false;

    /**
     * constructor
     * @param integer $surveyId
     * @param string|null $language
     * @throws Exception
     */
    public function __construct($surveyId, $language = null)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new Exception('Invalid survey id', 404);
        }
        if ($oSurvey->active != "Y") {
            throw new Exception('Invalid survey id', 404);
        }
        $this->surveyId = $surveyId;
        if (is_null($language)) {
            $language = $oSurvey->language;
        } elseif (!in_array($language, $oSurvey->getAllLanguages())) {
            throw new Exception("Invalid language", 400);
        }
        $this->language = $language;
        $this->aQuestionAnswers = \getQuestionInformation\helpers\surveyAnswers::getAllQuestionsAnswers($this->surveyId, $this->language);
        $this->oAvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($this->surveyId);
    }

    /**
     * Fill the export and the related export
     * Return the current export (Classic)
     * @return array|void
     */
    public function export()
    {
        /* fix answerCode */
        if ($this->answerCode && $this->answerFormat != 'code') {
            $this->answerFormat = 'code';
        }
        $oSurvey = Survey::model()->findByPk($this->surveyId);
        /* Construction of header */
        /* Global column */
        $aHeader = array(
            'id' => gT("Response ID"),
        );
        if ($oSurvey->datestamp == "Y") {
            $aHeader['submitdate'] = gT("Date submitted");
        } else {
            $aHeader['submitdate'] = gT("Completed");
        }
        $aHeader['lastpage'] = gT("Last page");
        $aHeader['startlanguage'] = gT("Start language");
        $aHeader['seed'] = gT("Seed");
        if ($oSurvey->anonymized != "Y") {
            $aHeader['token'] = gT("Token");
        }
        if ($oSurvey->datestamp == "Y") {
            $aHeader['startdate'] = gT("Date started");
            $aHeader['datestamp'] = gT("Date last action");
        }
        if ($oSurvey->ipaddr == "Y") {
            $aHeader['ipaddr'] = gT("IP address");
        }
        if ($oSurvey->refurl == "Y") {
            $aHeader['refurl'] = gT("Referrer URL");
        }
        /* The question columns */
        $columnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($this->surveyId, $this->language);
        $columnsInformation->ByEmCode = true;
        $allQuestionListData = $columnsInformation->allQuestionListData();

        $aQuestionsTitle = array_map(function ($questionData) {
            return $questionData['title'];
        }, $allQuestionListData['options']);
        $aHeader = array_merge($aHeader, $aQuestionsTitle);

        if (!empty($this->aColumns)) {
            $aHeader = array_intersect_key($aHeader, array_fill_keys($this->aColumns, null));
        }
        $this->startExport($aHeader);
        $aCodeToColumnWithDefault = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($this->surveyId, null, true));

        /* Get the data of current survey */
        $oCriteria = new CDbCriteria();
        $oCriteria->order = "id";
        // Can not restrict select : possible relation with childs
        if ($this->completed) {
            $oCriteria->addCondition("submitdate IS NOT NULL");
        }
        if ($this->token) {
            if ($this->tokenGroup && version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
                $oCriteria->addInCondition('token', \TokenUsersListAndManagePlugin\Utilities::getTokensList($this->surveyId, $this->token, false));
            } else {
                $oCriteria->compare('token', $this->token);
            }
        }
        if ($this->responseId) {
            $oCriteria->compare('id', $this->responseId);
            $this->disableIdCheck = false;
        } elseif (!empty($this->responseIds)) {
            $oCriteria->addInCondition('id', $this->responseIds);
            $this->disableIdCheck = false;
        }
        $ResponseModel = \Response::model($this->surveyId);
        $oResponses = $ResponseModel->findAll($oCriteria);
        $aCodeToColumn = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($this->surveyId));
        foreach ($oResponses as $oResponse) {
            $aData = array();
            foreach ($aHeader as $code => $text) {
                $column = $code;
                if (isset($aCodeToColumn[$code])) {
                    $column = $aCodeToColumn[$code];
                }
                if (isset($oResponse->$column) && $oResponse->$column !== '') {
                    $type = isset($this->aQuestionAnswers[$column]) ? $this->aQuestionAnswers[$column]['type'] : null;
                    $value = $oResponse->$column;
                    switch ($type) {
                        case 'decimal':
                            if ($value && $value[0] == ".") {
                                $value = "0" . $value;
                            }
                            if (strpos($value, ".")) {
                                $value = rtrim(rtrim($value, "0"), ".");
                            }
                            break;
                        case 'answer':
                            if ($this->answerFormat == 'full' && isset($this->aQuestionAnswers[$column]['answers'][$value])) {
                                $value = $this->aQuestionAnswers[$column]['answers'][$value];
                            }
                            if ($this->answerFormat == 'both') {
                                if (isset($this->aQuestionAnswers[$column]['answers'][$value])) {
                                    $value = "[" . $value . "]" . $this->aQuestionAnswers[$column]['answers'][$value];
                                } else {
                                    $value = "[" . $value . "]";
                                }
                            }
                            break;
                        case 'freeanswer':
                            // No change
                            break;
                        default:
                            if ($this->answerFormat == 'code' && $column == 'submitdate' && $oSurvey->datestamp != "Y") {
                                if ($value) {
                                    $value = gT("Yes");
                                }
                            }
                            // No change ?
                    }
                    $aData[$column] = $value;
                } else {
                    $aData[$column] = "";
                }
            }
            $this->addExportData($aData);
            if (!$this->disableIdCheck) {
                $this->addRelated($oResponse);
            }
        }
        if ($this->disableIdCheck) {
            $this->addRelated();
        }
        return $this->endExportReponse();
    }

    /**
     * Set extra survey export
     * @param integer $surveyId
     * @param string $sCode
     * @param string[]
     * @return void
     */
    public function addRelatedExport($surveyId, $sCode, $aRelatedExports, $options = array())
    {
        $aChildrensSurveyExport = $this->oAvailableRelatedExport->getChildrenSurveyExports();
        if (isset($aChildrensSurveyExport[$surveyId])) {
            $aChildSurveyExport = $aChildrensSurveyExport[$surveyId];
            $this->aRelatedExports[$surveyId] = array();
            foreach ($aRelatedExports as $export) {
                if (isset($aChildrensSurveyExport[$surveyId][$export])) {
                    $this->aRelatedExports[$surveyId][$export] = $aChildrensSurveyExport[$surveyId][$export];
                    $this->aRelatedExportsOptions[$surveyId][$export] = $options;
                }
            }
            if (!empty($this->aRelatedExports)) {
                $this->aRelatedCode[$surveyId] = $sCode;
            }
        }
    }
    /**
     * Start export
     * @param $aHeader
     */
    protected function startExport($aHeader)
    {
        switch ($this->dataHeader) {
            case 'text':
                // Nothing to do
                break;
            case 'both':
                $newaHeader = [];
                foreach ($aHeader as $key => $text) {
                    $newaHeader[$key] = $key . "\n" . $text;
                }
                $aHeader = $newaHeader;
                break;
            case 'none':
            case 'code':
            default:
                $aHeader = array_keys($aHeader);
        }
        $this->aHeader = $aHeader;
    }

    /**
     * Add data line
     * @param string[] $aData
     */
    protected function addExportData($aData)
    {
        if ($this->dataHeader == 'none') {
            $this->aDatas[] = array_values($aData);
        } else {
             $this->aDatas[] = array_combine($this->aHeader, $aData);
        }
    }

    /**
     * Add the related export :
     * @param \oResponse|null $oResponse
     */
    protected function addRelated($oResponse = null)
    {
        $aChildrensSurveyRestriction = $this->oAvailableRelatedExport->getChildrensSurveyRestriction();
        if (is_null($this->responseIdsByRelated)) {
            $this->responseIdsByRelated = array();
        }
        $aResponseIdsByRelated = $this->responseIdsByRelated;
        foreach ($aChildrensSurveyRestriction as $extraSurveyId => $aRestrictions) {
            $oExtraSurvey = Survey::model()->findByPk($extraSurveyId);
            if ($oExtraSurvey->active != 'Y') {
                // @todo : log as error
                continue;
            }
            if (empty($this->token) && empty($this->responseId) && empty($this->responseIds)) {
                $aResponseIdsByRelated[$extraSurveyId] = true;
                continue;
            }
            $oCriteria = new CDbCriteria();
            $oCriteria->order = "id";
            $aSelect = array(
                'id',
                'submitdate',
            );
            if (is_null($oResponse) && $this->token) {
                if ($aRestrictions['token'] && $oExtraSurvey->anonymized != 'Y') {
                    if ($this->tokenGroup && version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
                        $oCriteria->addInCondition('token', \TokenUsersListAndManagePlugin\Utilities::getTokensList($this->surveyId, $this->token, false));
                    } else {
                        $oCriteria->compare('token', $oResponse->token);
                    }
                    $aSelect[] = 'token'; // needed by MSSQL
                }
                foreach ($aRestrictions['other'] as $extraColumn => $restrict) {
                    $aValuesRestricted = array();
                    $aFixeds = array_filter(array_unique($restrict['fixeds']));
                    foreach ($aFixeds as $fixed) {
                        $aValuesRestricted[] = $fixed;
                    }
                    if (!empty($aValuesRestricted)) {
                        $oCriteria->addInCondition(Yii::app()->getDb()->quoteColumnName($extraColumn), $aValuesRestricted);
                        $aSelect[] = Yii::app()->getDb()->quoteColumnName($extraColumn); // needed by MSSQL
                    }
                }
            } else {
                if ($aRestrictions['id'] && $oResponse) {
                    $oCriteria->compare(Yii::app()->getDb()->quoteColumnName($aRestrictions['id']), $oResponse->getAttribute("id"));
                }
                if ($aRestrictions['token'] && $oExtraSurvey->anonymized != 'Y' && isset($oResponse->token)) {
                    if ($this->tokenGroup && version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
                        $oCriteria->addInCondition('token', \TokenUsersListAndManagePlugin\Utilities::getTokensList($this->surveyId, $oResponse->token, false));
                    } else {
                        $oCriteria->compare('token', $oResponse->token);
                    }
                    $aSelect[] = 'token'; // needed by MSSQL
                }
                foreach ($aRestrictions['other'] as $extraColumn => $restrict) {
                    $aValuesRestricted = array();
                    $aFixeds = array_filter(array_unique($restrict['fixeds']));
                    foreach ($aFixeds as $fixed) {
                        $aValuesRestricted[] = $fixed;
                    }
                    $aColumns = array_filter(array_unique($restrict['columns']));
                    foreach ($aColumns as $column) {
                        if (isset($oResponse->$column)) {
                            $aValuesRestricted[] = $oResponse->getAttribute($column); // And if empty ?
                        }
                    }
                    if (!empty($aValuesRestricted)) {
                        $oCriteria->addInCondition(Yii::app()->getDb()->quoteColumnName($extraColumn), $aValuesRestricted);
                        $aSelect[] = Yii::app()->getDb()->quoteColumnName($extraColumn); // needed by MSSQL
                    }
                }
            }
            // Check select count ????
            $oRelatedResponse = \Response::model($extraSurveyId)->findAll($oCriteria);
            if (empty($aResponseIdsByRelated[$extraSurveyId])) {
                $aResponseIdsByRelated[$extraSurveyId] = array();
            }
            $aResponseIdsByRelated[$extraSurveyId] = array_unique(array_merge($aResponseIdsByRelated[$extraSurveyId], \CHtml::listData($oRelatedResponse, 'id', 'id')));
        }
        $this->responseIdsByRelated = $aResponseIdsByRelated;
    }

    /**
     * Export the related data as array
     */
    public function exportRelated()
    {
        if (is_null($this->responseIdsByRelated)) {
            throw new Exception('You must export before export related.');
        }
        $aRelated = array();
        foreach ($this->aRelatedCode as $extraSurveyId => $code) {
            if (empty($this->aRelatedExports[$extraSurveyId])) {
                continue;
            }

            $aRelated[$code] = array();
            foreach ($this->aRelatedExports[$extraSurveyId] as $export => $label) {
                $options = isset($this->aRelatedExportsOptions[$extraSurveyId][$export]) ? $this->aRelatedExportsOptions[$extraSurveyId][$export] : array();
                if (empty($this->responseIdsByRelated[$extraSurveyId]) && ( empty($options['alltotal']) || empty($options['totalforced']) )) {
                    $aRelated[$code][$export] = array();
                    continue;
                }
                if ($export == 'export' || $export == 'exportall') {
                    $exportResponse = new \ExportResponseWithRelated\exports\Responses($extraSurveyId, $this->language);
                    $exportResponse->dataHeader = $this->dataHeader;
                    if (is_array($this->responseIdsByRelated[$extraSurveyId]) || is_int($this->responseIdsByRelated[$extraSurveyId])) {
                        $exportResponse->responseIds = $this->responseIdsByRelated[$extraSurveyId];
                    }
                    $exportResponse->completed = ($export == 'export');
                    if (isset($this->aChildsColumns[$extraSurveyId])) {
                        $exportResponse->aColumns = $this->aChildsColumns[$extraSurveyId];
                    }
                    $aRelated[$code][$export] = $exportResponse->export();
                }
                if (substr($export, 0, strlen('filtered_')) == 'filtered_' || substr($export, 0, strlen('filteredall_')) == 'filteredall_') {
                    $completed = true;
                    $compile = substr($export, strlen('filtered_'));
                    if (substr($export, 0, strlen('filteredall_')) == 'filteredall_') {
                        $compile = substr($export, strlen('filteredall_'));
                        $completed = false;
                    }
                    $filteredExport = new \filteredAdaptedExport\exports\filteredExport($extraSurveyId, $this->language);
                    $filteredExport->dataHeader = $this->dataHeader;
                    if (is_array($this->responseIdsByRelated[$extraSurveyId]) || is_int($this->responseIdsByRelated[$extraSurveyId])) {
                        $filteredExport->responseIds = $this->responseIdsByRelated[$extraSurveyId];
                    }
                    if (empty($this->responseIdsByRelated[$extraSurveyId])) {
                        $filteredExport->none = true;
                    }
                    $filteredExport->completed = $completed;
                    /* Set the settings (and the compile) */
                    $questionCompile = \ExportResponseWithRelated\Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionCompile');
                    $questionsCompile = \ExportResponseWithRelated\Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionsCompile');
                    if (is_array($questionsCompile)) {
                        $questionsCompileLower = array_map('strtolower', $questionsCompile);
                        if (in_array(strtolower($compile), $questionsCompileLower)) {
                            $key = array_search(strtolower($compile), $questionsCompileLower);
                            $questionCompile = $questionsCompile[$key];
                        }
                    }
                    $filteredExport->setQuestionCompile($questionCompile);
                    $filteredExport->options = array_merge($filteredExport->options, $options);
                    $filteredExport->setQuestionCompileBy(\ExportResponseWithRelated\Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionCompileBy'));
                    $typesQuestions = array(
                        'questionsExportData',
                        'questionsExportCount',
                        'questionsExportSum',
                        'questionsExportAverage',
                        'questionsExportAverageWeighted',
                        'questionsExportAverageWeightedAlt',
                    );
                    foreach ($typesQuestions as $typeQuestions) {
                        $setting = \ExportResponseWithRelated\Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, $typeQuestions);
                        if (!empty($setting)) {
                            $filteredExport->setQuestionsByType($typeQuestions, $setting);
                        }
                    }
                    $filteredExport->setQuestionExportWeight(\ExportResponseWithRelated\Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionExportWeight'));
                    $filteredExport->setQuestionExportWeightAlt(\ExportResponseWithRelated\Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionExportWeightAlt'));
                    $filteredExport->dataHeader = $this->dataHeader;
                    $filteredExport->exportHeader = $this->dataHeader;
                    $filteredExport->exportAnswers = 'text'; // todo : add it here (current is text)
                    $aRelated[$code][$export] = $filteredExport->export();
                }
            }
        }
        return $aRelated;
    }
    /**
     * End the export
     * return array|void
     */
    protected function endExportReponse()
    {
        return $this->aDatas;
    }
}
