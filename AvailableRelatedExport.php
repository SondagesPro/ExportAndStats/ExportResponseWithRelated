<?php

/**
 * Get the availabmle related export
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.6.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace ExportResponseWithRelated;

use Yii;
use CDbCriteria;
use PDO;
use Exception;
use Survey;
use Question;
use QuestionAttribute;
use Plugin;
use PluginSetting;

class AvailableRelatedExport
{
    /**
     * @var null|array[]
     * Array of related plugins with plugin name as key
     * current plugin value are attribute used for:
     * - question-srid : quetion used for srid
     * - question-useid : find only srid, not set mean true
     * - questions-others : used for other relation : allow to find uniqueId
     * - token-usage : attribute for way of using token : no|token|group , default is token. If not set : token. (used in spreadsheetSurveyTokenUsage)
     * - token-group-usage : attribute for way of using token : no|token|group , default is true. If not set : false (used in extraSurveyResponseListAndManage)
     */
    private static $availableRelatedPlugins;

    /**
     * @var integer survey id
     */
    public $surveyId;

    /**
     * @var null|integer[] the related survey id
     */
    private $aChildrensSurveys;

    /**
     * @var null|string[] qid for key, plugin for value
     */
    private $aPluginByQid;

    /**
     * The restriction by survey
     * @var array[]
     */
    private $aSurveysRestriction;

    /**
     * Return the related plugin
     * @todo add this to plugin via a function in plugins or
     * Any other solution ? New event ?
     */
    public static function availableRelatedPlugins()
    {
        if (is_null(self::$availableRelatedPlugins)) {
            self::$availableRelatedPlugins = array(
                /* questionExtraSurvey plugin */
                'extraSurvey' => array(
                    'surveyid' => 'extraSurvey',
                    'question-srid' => 'extraSurveyQuestionLink',
                    'question-useid' => 'extraSurveyQuestionLinkUse',
                    'questions-others' => 'extraSurveyOtherField',
                    'token-usage' => 'extraSurveyTokenUsage',
                ),
                'spreadsheetSurvey' => array(
                    'surveyid' => 'spreadsheetSurvey',
                    'question-srid' => 'spreadsheetSurveyQuestionLink',
                    'question-useid' => null,
                    'questions-others' => 'spreadsheetSurveyOtherField',
                    'token-usage' => 'spreadsheetSurveyTokenUsage',
                ),
            );
        }
        return self::$availableRelatedPlugins;
    }

    /**
     * constructor
     * @param integer survey id
     * @throw Exception
     */
    public function __construct($surveyId)
    {
        $this->surveyId = $surveyId;
        $oSurvey = Survey::model()->findByPk($this->surveyId);
        if (!$oSurvey) {
            throw new Exception(404, 'Invalid survey id');
        }
    }

    /**
     * Return the surveys list with relation (child of)
     * @return integer[]
     */
    public function getChildrenSurveyExports()
    {
        $this->setAvailableSurveys();
        /* Get possible question list */
        $aChildrenSurveyExports = array();
        foreach ($this->aChildrensSurveys as $extraSurveyId) {
            $aChildrensSurveys[$extraSurveyId] = array();
            foreach ($aChildrensSurveys as $extraSurveyId => $aChildrensSurvey) {
                $exports = array(
                    'export' => Utilities::translate("Classic export (submitted responses)"),
                    'exportall' => Utilities::translate("Classic export (all responses)"),
                );
                if (\ExportResponseWithRelated\Utilities::getPluginId('filteredAdaptedExport')) {
                    $sQuestionCompile = Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionCompile');
                    if ($sQuestionCompile) {
                        $exports['filtered_' . $sQuestionCompile] = sprintf(Utilities::translate("Filtered export, using %s for filter (submitted responses)."), $sQuestionCompile);
                        $exports['filteredall_' . $sQuestionCompile] = sprintf(Utilities::translate("Filtered export, using %s for filter (all responses)."), $sQuestionCompile);
                    }
                    $aQuestionsCompile = Utilities::getSurveyPluginSetting('filteredAdaptedExport', $extraSurveyId, 'questionsCompile');
                    if ($aQuestionsCompile) {
                        foreach ($aQuestionsCompile as $sQuestionCompile) {
                            $exports['filtered_' . $sQuestionCompile] = sprintf(Utilities::translate("Filtered export, using %s for filter (submitted responses)."), $sQuestionCompile);
                            $exports['filteredall_' . $sQuestionCompile] = sprintf(Utilities::translate("Filtered export, using %s for filter (all responses)."), $sQuestionCompile);
                        }
                    }
                }
                $aChildrenSurveyExports[$extraSurveyId] = $exports;
            }
        }
        return $aChildrenSurveyExports;
    }

    /**
     * Return the surveys list with relation (child of)
     * @return array : key for survey, array with id as boolean, token ad boolean, other are in project (not done currently)
     */
    public function getChildrensSurveyRestriction()
    {
        if (!is_null($this->aSurveysRestriction)) {
            return $this->aSurveysRestriction;
        }
        $this->setAvailableSurveys();
        $availableRelatedPlugins = self::availableRelatedPlugins();
        $aSurveysRestriction = array();
        $oSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($this->surveyId));
        foreach ($this->aPluginByQid as $qid => $plugin) {
            $aAttributes = $availableRelatedPlugins[$plugin];
            $extraSurveyId = Utilities::getQuestionAttribute($qid, $aAttributes['surveyid']);
            if (!$extraSurveyId && !Utilities::isSurveyIsValid($extraSurveyId)) {
                // Securing, must not happen
            }
            if (empty($aSurveysRestriction[$extraSurveyId])) {
                $aSurveysRestriction[$extraSurveyId] = array(
                    'count' => 0, // Number of qid
                    'id' => null,
                    'token' => null,
                    'other' => array(),
                );
            }
            $aSurveysRestriction[$extraSurveyId]['count']++;
            $oExtraSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($extraSurveyId));
            /* id relation */
            $sridLink = false;
            $sridLinkCode = Utilities::getQuestionAttribute($qid, $aAttributes['question-srid']);
            if ($sridLinkCode) {
                /* Find it in oExtraSurveyQuestionCode */
                if (isset($oExtraSurveyQuestionCode[$sridLinkCode])) {
                    $sridLink = $oExtraSurveyQuestionCode[$sridLinkCode];
                }
            }
            $useId = true;
            if ($aAttributes['question-useid']) {
                $useId = boolval(Utilities::getQuestionAttribute($qid, $aAttributes['question-useid'], true));
            }
            // Only one question without srid : mean no relation
            if (!$sridLink || !$useId) {
                $aSurveysRestriction[$extraSurveyId]['id'] = false;
            } elseif ($aSurveysRestriction[$extraSurveyId]['id'] !== false) {
                if ($aSurveysRestriction[$extraSurveyId]['id'] != $sridLink) {
                    // Todo : log it or alert admin user
                }
                $aSurveysRestriction[$extraSurveyId]['id'] = $sridLink;
            }
            /* token */
            $tokenLink = Utilities::getQuestionAttribute($qid, $aAttributes['token-usage'], 'token');
            if (!$tokenLink) { // ===""
                $aSurveysRestriction[$extraSurveyId]['token'] = false;
            } elseif ($aSurveysRestriction[$extraSurveyId]['token'] !== false) {
                // @todo : must not set token if already group
                $aSurveysRestriction[$extraSurveyId]['token'] = $tokenLink;
            }
            /* Others */
            $othersLink = trim(Utilities::getQuestionAttribute($qid, $aAttributes['questions-others'], null));
            if ($othersLink) {
                $fixedOtherLinks = $this->getFixedOtherField($othersLink, $oSurveyQuestionCode, $extraSurveyId);
                $aSurveysRestriction[$extraSurveyId]['other'] = array_merge_recursive($aSurveysRestriction[$extraSurveyId]['other'], $fixedOtherLinks);
            }
        }
        /* Remove other selection if number of params is lesser than count */
        foreach (array_keys($aSurveysRestriction) as $extraSurveyId) {
            foreach (array_keys($aSurveysRestriction[$extraSurveyId]['other']) as $column) {
                $aValues = $aSurveysRestriction[$extraSurveyId]['other'][$column];
                if (count($aValues['fixeds']) + count($aValues['columns']) < $aSurveysRestriction[$extraSurveyId]['count']) {
                    unset($aSurveysRestriction[$extraSurveyId]['other'][$column]);
                } else {
                    $aSurveysRestriction[$extraSurveyId]['other'][$column]['fixeds'] = array_unique($aValues['fixeds']);
                    $aSurveysRestriction[$extraSurveyId]['other'][$column]['columns'] = array_unique($aValues['columns']);
                }
            }
        }
        $this->aSurveysRestriction = $aSurveysRestriction;
        return $this->aSurveysRestriction;
    }

    /**
     * Return the id of question for key and surveyid for
     * @return integer[]
     */
    private function setAvailableSurveys()
    {
        if (is_array($this->aChildrensSurveys) && is_array($this->aPluginByQid)) {
            return;
        }
        $this->aChildrensSurveys = array();
        $this->aPluginByQid = array();

        $availablePlugins = self::availableRelatedPlugins();
        $surveyAttributes = array_map(function ($availablePlugin) {
            return $availablePlugin['surveyid'];
        }, $availablePlugins);

        $oSurvey = Survey::model()->findByPk($this->surveyId);
        $surveyId = $this->surveyId;
        $language = $oSurvey->language;
        $questionTable = Question::model()->tableName();
        $command = Yii::app()->db->createCommand()
            ->select("qid,{{groups}}.group_order, {{questions}}.question_order")
            ->from($questionTable)
            ->where("({{questions}}.sid = :sid AND {{questions}}.parent_qid = 0) AND ({{questions}}.type = 'X' OR {{questions}}.type = 'T')")
            ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid")
            ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
            ->bindParam(":sid", $surveyId, PDO::PARAM_INT);
        /* 3X compat not broken */
        if (intval(App()->getConfig('versionnumber')) < 4) {
            $command = Yii::app()->db->createCommand()
                ->select("qid,{{questions}}.language as language,{{groups}}.group_order, {{questions}}.question_order")
                ->from($questionTable)
                ->where("({{questions}}.sid = :sid AND {{questions}}.language = :language AND {{questions}}.parent_qid = 0) AND ({{questions}}.type = 'X' OR {{questions}}.type = 'T')")
                ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid  AND {{questions}}.language = {{groups}}.language")
                ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
                ->bindParam(":sid", $surveyId, PDO::PARAM_INT)
                ->bindParam(":language", $language, PDO::PARAM_STR);
        }
        $aPossibleQuestions = $command->query()->readAll();

        foreach ($aPossibleQuestions as $aQuestion) {
            $criteria = new CDbCriteria();
            $criteria->compare('qid', $aQuestion['qid']);
            $criteria->addCondition(App()->db->quoteColumnName('value') . " <> ''");
            $criteria->addInCondition('attribute', $surveyAttributes);
            $oQuestionAttributeSurvey = QuestionAttribute::model()->find($criteria);
            if ($oQuestionAttributeSurvey) {
                $extraSurveyId = $oQuestionAttributeSurvey->value;
                /* @todo : Check validity of survey */
                if (Utilities::isSurveyIsValid($extraSurveyId)) {
                    $this->aChildrensSurveys[] = $extraSurveyId;
                    if (!empty($this->aPluginByQid[$aQuestion['qid']])) {
                        // TODO : add a error/warning log
                    }
                    $this->aPluginByQid[$aQuestion['qid']] = $oQuestionAttributeSurvey->getAttribute('attribute');
                }
            }
        }
    }

    /**
     * Fix other field : check for available other fields
     * return array : [column=>[fixeds =>[], columns =[]] ]
     */

    private function getFixedOtherField($otherField, $aCodeToColumns, $extraSurveyId)
    {
        $aOtherFieldsLines = preg_split('/\r\n|\r|\n/', $otherField, -1, PREG_SPLIT_NO_EMPTY);
        $aOtherFields = array();
        $oExtraSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($extraSurveyId));
        $oExtraSurveyQuestionCode['token'] = 'token'; // Can use token
        foreach ($aOtherFieldsLines as $otherFieldLine) {
            if (!strpos($otherFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($otherFieldLine, 0, strpos($otherFieldLine, ":"));
            $value = substr($otherFieldLine, strpos($otherFieldLine, ":") + 1);
            if (isset($oExtraSurveyQuestionCode[$key]) && $value) {
                $relatedColumn = $oExtraSurveyQuestionCode[$key];
                if (!isset($aOtherFields[$relatedColumn])) {
                    $aOtherFields[$relatedColumn] = array(
                        'fixeds' => array(),
                        'columns' => array(),
                    );
                }
                if (strpos($value, "{") === false && strpos($value, "}") === false) {
                    $aOtherFields[$relatedColumn]['fixeds'][] = $value;
                }
                if ($value[0] == "{" && $value[-1] == "}") {
                    $code = substr($value, 1, -1);
                    if (substr($code, -5) === ".NAOK") {
                        $code = substr($code, 0, -5);
                    }
                    if (isset($aCodeToColumns[$code])) {
                        $aOtherFields[$relatedColumn]['columns'][] = $aCodeToColumns[$code];
                    } elseif ($code == "TOKEN" || $code == "TOKEN:TOKEN") {
                        $aOtherFields[$relatedColumn]['columns'][] = 'token';
                    } else {
                        /* @todo : warning */
                    }
                }
            }
        }
        return $aOtherFields;
    }
}
