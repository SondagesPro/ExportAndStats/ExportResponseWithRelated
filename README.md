# ExportResponseWithRelated

A tool for other export : Export each response with related survey.

By default export response in array that can be used for other plugin directly, can export related data at same time.

## Classic export

The plugin can use [filteredAdaptedExport](https://gitlab.com/SondagesPro/ExportAndStats/filteredAdaptedExport)

```
$exportResponse = new \ExportResponseWithRelated\exports\Responses($surveyId,$language);
$exportResponse->responseId = $this->responseId;
$aExportDatas = $exportResponse->export();
```

## Available related responses

- Response from questionExtraSurvey plugin
- Response from questionSpreadsheetSurvey plugin

The related export need to be set using `$exportResponse->addRelatedExport` function.

You can have the list of related export with
```
$oAvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($surveyId);
$aChildrensSurveyExport = $oAvailableRelatedExport->getChildrenSurveyExports();
```

## Other export by other plugins

This plugin can use [filteredAdaptedExport](https://gitlab.com/SondagesPro/ExportAndStats/filteredAdaptedExport) if this plugin is available.

All know export type are listed in ExportResponseWithRelated::getChildrenSurveyExports functions

## Relation used for export

All relation made in survey are used, if same survey are used more than one time : selection is done using the 2 questions settings.

## Home page & Copyright

- HomePage <https://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu <https://sondages.pro>
- Copyright © 2020 OECD
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)

Distributed under [GNU AFFERO PUBLIC LICENSE Version 3](https://gnu.org/licenses/agpl-3.0.txt) licence
