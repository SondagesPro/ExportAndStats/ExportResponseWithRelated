<?php

/**
 * ExportResponseWithRelated Plugin for LimeSurvey
 * Export data and related data as array
 * This i a tool for other plugins
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://sondages.pro>
 * @copyright 2020-2024 OECD
 * @license AGPL v3
 * @version 0.7.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Affero Public License for more details.
 *
 */
class ExportResponseWithRelated extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $name = 'ExportResponseWithRelated';
    protected static $description = 'Export related data as array : a tool for other plugins';

    /** @inheritdoc **/
    public $allowedPublicMethods = [
        'actionExport',
        'actionPermission',
        'actionSurveysSettings'
    ];

    /** @inheritdoc **/
    public function init()
    {
        /* Helper */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        /* Language (by name) */
        $this->subscribe('afterPluginLoad');
        /* Allow export (by post or get */
        $this->subscribe('newDirectRequest', 'directExport');
        $this->subscribe('beforeSurveySettings');

        /* Add tool button */
        $this->subscribe('beforeToolsMenuRender');
    }

    /** @inheritdoc
     * @see event afterPluginLoad
     **/
    public function afterPluginLoad()
    {
        $messageSource = array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this) . 'Lang',
            'cachingDuration' => 0,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'locale',
            'catalog' => 'messages',// default from Yii
        );
        Yii::app()->setComponent(get_class($this) . 'Messages', $messageSource);
    }

    public function beforeToolsMenuRender()
    {
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        if (!Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export')) {
            return;
        }
        $AvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($surveyId);
        $childrenSurveyExports = $AvailableRelatedExport->getChildrenSurveyExports();
        if (empty($childrenSurveyExports)) {
            return;
        }
        $aMenuItem = array(
            'label' => $this->translate('Export survey and childs'),
            'iconClass' => 'fa fa-arrow-circle-o-right',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionExport',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $event->append('menuItems', array($menuItem));
    }

    /**
     * Expport action : show form or export if allowed
     * @param integer $surveyId
     * @throws Exception
     */
    public function actionExport($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export')) {
            throw new CHttpException(403);
        }
        /* Get the current survey language */
        $language =  $oSurvey->language;
        if (App()->getRequest()->getPost('export' . get_class($this)) == 'export') {
            $this->export();
        }
        if (App()->getRequest()->getPost('saveasdefault' . get_class($this)) == 'saveasdefault') {
            $this->saveDefaultExport($surveyId);
        }
        /* Basic data */
        $aData = $this->getBasicData($surveyId);

        $aLanguagesCode = Survey::model()->findByPk($surveyId)->getAllLanguages();
        $aLanguages = array();
        foreach ($aLanguagesCode as $sLanguage) {
            $aLanguages[$sLanguage] = getLanguageNameFromCode($sLanguage, false);
        }
        $aExportSettings[$this->translate('General')] = array(
            'completed' => [
                'type' => 'select',
                'label' => $this->translate('Completion state'),
                'help' => $this->translate('Use completed=Y parameters in URL or form'),
                'options' => [
                    'Y' => $this->translate("Completed responses only"),
                ],
                'htmlOptions' => [
                    'empty' => $this->translate("All responses"),
                ],
                'current' => $this->get('completed', 'Survey', $surveyId, '')
            ],
            'exportlang' => [
                'type' => 'select',
                'label' => $this->translate('Export language'),
                'options' => $aLanguages,
                'help' => $this->translate('Use exportlang=[language code] parameters in URL or form'),
                'current' => $this->get('exportlang', 'Survey', $surveyId, $oSurvey->language),
            ],
            'filename' => [
                'type' => 'string',
                'label' => $this->translate('File name'),
                'help' => $this->translate('You can force the file name for excel.'),
                'current' => $this->get('filename', 'Survey', $surveyId, ''),
                'htmlOptions' => [
                    'placeholder' => 'results-survey-children-' . $surveyId
                ]
            ],
        );
        $aExportSettings[$this->translate('Format')] = array(
            'answerCode' => [
                'type' => 'select',
                'label' => $this->translate('Export responses as'),
                'options' => [
                    'full' => $this->translate("Full answers"),
                    'code' => $this->translate("Answer codes"),
                    'both' => $this->translate("Answer codes in bracket followed by full answers"),
                ],
                'help' => $this->translate('Use answerCode=(full|code|both) parameters in URL or form'),
                'current' => $this->get('answerCode', 'Survey', $surveyId, 'code')
            ],
            'answerHeader' => [
                'type' => 'select',
                'label' => $this->translate('Export questions as'),
                'options' => [
                    'text' => $this->translate("Full question text"),
                    'code' => $this->translate("Question code"),
                    'both' => $this->translate("Question codes followed by question text on next line (line feed)"),
                    'none' => $this->translate("None"),
                ],
                'help' => $this->translate('Use answerHeader=(text|code|none) parameters in URL or form'),
                'current' => $this->get('answerHeader', 'Survey', $surveyId, 'code')
            ],
            'format' => [
                'type' => 'select',
                'label' => $this->translate('Export format'),
                'options' => [
                    'xlsx' => $this->translate("Microsoft Excel"),
                    'json' => $this->translate("JSON"),
                ],
                'help' => $this->translate('Use format=(xlsx|json) parameters in URL or form'),
                'current' => $this->get('format', 'Survey', $surveyId, 'xlsx')
            ],
            'fancySheetName' => [
                'type' => 'select',
                'label' => $this->translate('Use sheet name with survey name'),
                'options' => [
                    'Y' => $this->translate("Yes"),
                    'N' => $this->translate("No"),
                ],
                'help' => $this->translate('Use fancySheetName=(Y|N) parameters in URL or form'),
                'current' => $this->get('fancySheetName', 'Survey', $surveyId, 'N')
            ],
        );

        $aData['title'] = $this->translate("Export response with childs");
        $aData['warningString'] = null;
        $aData['aExportSettings'] = $aExportSettings;
        $aData['settingview'] = 'exportsettings';
        $aData['activetab'] = 'export';
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /**
     * Set the settings for permission
     * @param integer $surveyId
     * @throws Exception
     */
    public function actionPermission($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $language =  $oSurvey->language;
        if (App()->getRequest()->getPost('save' . get_class($this)) == 'save') {
            $this->savePermissionSettings($surveyId);
        }
        /* Basic data */
        $aData = $this->getBasicData($surveyId);
        /* Permission settings */
        $currentSridUrl = App()->createUrl(
            "plugins/direct",
            array(
                "plugin" => 'ExportResponseWithRelated',
                "function" => 'export',
                "surveyid" => '{SID}',
                "srid" => '{SAVEDID}',
            )
        );
        $currentSridUrl = str_replace(['%7B','%7D'], ['{','}'], $currentSridUrl);
        $currentTokenUrl = App()->createUrl(
            "plugins/direct",
            array(
                "plugin" => 'ExportResponseWithRelated',
                "function" => 'export',
                "surveyid" => '{SID}',
                "token" => '{TOKEN}',
            )
        );
        $currentTokenUrl = str_replace(['%7B','%7D'], ['{','}'], $currentTokenUrl);
        $aSettings[$this->translate('Permission')] = array(
            'tokenPermission' => [
                'type' => 'select',
                'label' => $this->translate('Allow token to export'),
                'options' => [
                    'none' => $this->translate("Disable"),
                    'token' => $this->translate("Only same token"),
                    'manager' => $this->translate("Only group manager (same group)"),
                    'restricted' => $this->translate("Restricted to response with access (for restricted user), all for other particpants"),
                    'all' => $this->translate("All (same group)"),
                ],
                'help' => sprintf($this->translate('You can allow token user to export by token, or by list of id. Access control are done only for this surey (parent). Sample url : %s'), "<code>" . $currentTokenUrl . "</code>"),
                'current' => $this->get('tokenPermission', 'Survey', $surveyId, 'none')
            ],
            'disableIdCheck' => [
                'type' => 'select',
                'label' => $this->translate('Only token export'),
                'options' => [
                    'Y' => gT('Yes'),
                    'N' => gT('No'),
                ],
                'help' => $this->translate('When you export by token, system check each response ID for current related survey, you can choose to use only token value. Speed of export are improved, but some response can be unrelated.'),
                'current' => $this->get('disableIdCheck', 'Survey', $surveyId, 'N')
            ],
            'currentSridPermission' => [
                'type' => 'select',
                'label' => $this->translate('Allow export current response.'),
                'options' => [
                    'Y' => $this->translate("Yes"),
                    'N' => $this->translate("No"),
                ],
                'help' => sprintf($this->translate('Allowing to export current response, sample URL %s.'), "<code>" . $currentSridUrl . "</code>"),
                'current' => $this->get('currentSridPermission', 'Survey', $surveyId, 'Y')
            ],
        );
        if (!self::extraPluginAllowTokenGroupPermission()) {
            unset($aSettings[$this->translate('Permission')]['tokenPermission']['options']['manager']);
            unset($aSettings[$this->translate('Permission')]['tokenPermission']['options']['restricted']);
            unset($aSettings[$this->translate('Permission')]['tokenPermission']['options']['all']);
        }
        $aData['title'] = $this->translate("Token permission settings");
        $aData['warningString'] = null;
        $aData['aSettings'] = $aSettings;
        $aData['settingview'] = 'permissionsettings';
        $aData['activetab'] = 'permission';
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /**
     * Save as default export
     */
    private function saveDefaultExport($surveyId)
    {
        $exportSettings = array(
            'completed',
            'exportlang',
            'filename',
            'answerCode',
            'answerHeader',
            'format',
            'fancySheetName'
        );
        foreach ($exportSettings as $setting) {
            $this->set($setting, App()->getRequest()->getPost($setting), 'Survey', $surveyId);
        }
    }

    /**
     * Set columns and other
     * @param integer $surveyId
     * @throws Exception
     */
    public function actionSurveysSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $language =  $oSurvey->language;
        if (App()->getRequest()->getPost('save' . get_class($this)) == 'save') {
            $this->saveSurveysSettings($surveyId);
        }
        /* Basic data */
        $aData = $this->getBasicData($surveyId);
        /* Construct the settings with children surveys */
        $aSettings = [];
        /* This survey */
        $aColumnsList = $this->getQuestionListDataForSettings($surveyId);
        $aSettings[$this->translate('This survey')] = array(
            'thisSurveyColumns' => [
                'type' => 'select',
                'label' => $this->translate('Survey columns to be exported'),
                'help' => $this->translate('Select the survey columns to be exported via this plugin. The settings use question title and subquestion code.'),
                'options' => $aColumnsList['data'],
                'htmlOptions' => array(
                    'multiple' => true,
                    'placeholder' => gT("All"),
                    'unselectValue' => "",
                    'options' => $aColumnsList['options'], // In dropdown, but not in select2
                ),
                'selectOptions' => array(
                    'placeholder' => gT("All"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class' => 'select2-withover select2-block-choice',
                ),
                'current' => $this->get('thisSurveyColumns', 'Survey', $surveyId)
            ],
            'sheetName' => [
                'type' => 'string',
                'label' => $this->translate('Sheet name'),
                'help' => $this->translate('Used if fancy sheet name is activated only.'),
                'current' => $this->get('sheetName', 'Survey', $surveyId, ''),
                'htmlOptions' => [
                    'placeholder' => $this->getFancySurveySheetname($surveyId),
                    'maxlength' => 31
                ]
            ],
        );
        $oAvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($surveyId);
        $aChildrensSurveyExport = $oAvailableRelatedExport->getChildrenSurveyExports();
        $aChildsSettings = [];
        $aChildsCurrentValues = $this->get('childSurveysColumns', 'Survey', $surveyId);
        $aChildsSheetNames = $this->get('sheetsName', 'Survey', $surveyId);
        foreach ($aChildrensSurveyExport as $childSurveyId => $exports) {
            $aColumnsList = $this->getQuestionListDataForSettings($childSurveyId);
            $aCurrentValues = [];
            if (isset($aChildsCurrentValues[$childSurveyId])) {
                $aCurrentValues = $aChildsCurrentValues[$childSurveyId];
            }
            $sCurrentSheetName = "";
            if (isset($aChildsSheetNames[$childSurveyId])) {
                $sCurrentSheetName = $aChildsSheetNames[$childSurveyId];
            }
            $label = "[" . $childSurveyId . "] " . Survey::model()->findByPk($childSurveyId)->getLocalizedTitle();
            $aChildsSettings['childSurveysColumns_' .  $childSurveyId] = array(
                'type' => 'select',
                'label' => $label,
                'help' => $this->translate('Select the survey columns to be exported via this plugin. The settings use question title and subquestion code relate to survey ID'),
                'options' => $aColumnsList['data'],
                'prefix' => 'childSurveysColumns',
                'htmlOptions' => array(
                    'multiple' => true,
                    'placeholder' => gT("All"),
                    'unselectValue' => "",
                    'options' => $aColumnsList['options'], // In dropdown, but not in select2
                ),
                'selectOptions' => array(
                    'placeholder' => gT("All"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class' => 'select2-withover select2-block-choice',
                ),
                'current' => $aCurrentValues
            );
            $aChildsSettings['sheetsName_' .  $childSurveyId] = array(
                'type' => 'string',
                'label' => sprintf($this->translate("Sheet name for %s"), $childSurveyId),
                'prefix' => 'sheetsName',
                'htmlOptions' => array(
                    'placeholder' => $this->getFancySurveySheetname($childSurveyId),
                    'maxlength' => 31
                ),
                'current' => $sCurrentSheetName
            );
        }
        $aSettings[$this->translate('Childs surveys columns')] = $aChildsSettings;
        $aData['title'] = $this->translate("Surveys settings");
        $aData['warningString'] = null;
        $aData['aSettings'] = $aSettings;
        $aData['settingview'] = 'surveyssettings';
        $aData['activetab'] = 'surveys';
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /**
     * Public call of export via direct request
     */
    public function directExport()
    {
        if ($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        $this->export();
    }

    /**
     * Save as default export
     */
    private function saveSurveysSettings($surveyId)
    {
        $surveySettings = array(
            'thisSurveyColumns',
            'sheetName'
        );
        foreach ($surveySettings as $setting) {
            $this->set($setting, App()->getRequest()->getPost($setting), 'Survey', $surveyId);
        }
        /* Child surveys */
        $surveysSettings = array(
            'childSurveysColumns',
            'sheetsName'
        );
        $oAvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($surveyId);
        $aChildrensSurveyExport = $oAvailableRelatedExport->getChildrenSurveyExports();
        foreach ($surveysSettings as $setting) {
            $setSettings = [];
            foreach ($aChildrensSurveyExport as $childSurveyId => $exports) {
                $setSettings[$childSurveyId] = App()->getRequest()->getPost($setting . '_' . $childSurveyId);
            }
            $this->set($setting, $setSettings, 'Survey', $surveyId);
        }
    }

    /**
     * Save permission settings
     */
    private function savePermissionSettings($surveyId)
    {
        $exportSettings = array(
            'tokenPermission',
            'disableIdCheck',
            'currentSridPermission',
        );
        foreach ($exportSettings as $setting) {
            $this->set($setting, App()->getRequest()->getPost($setting), 'Survey', $surveyId);
        }
    }

    /**
     * Export system
     * @param null|integer $surveyId
     */
    public function export($surveyId = null)
    {
        if (empty($surveyId)) {
            $surveyId = Yii::app()->getRequest()->getQuery(
                'sid',
                Yii::app()->getRequest()->getQuery(
                    'surveyid',
                    Yii::app()->getRequest()->getQuery('surveyId')
                )
            );
        }
        if ((string) (int) $surveyId !== (string) $surveyId) {
            throw new CHttpException(403, gT("Invalid survey id"));
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("Sorry, this survey was not found."));
        }
        if ($oSurvey->active != "Y") {
            throw new CHttpException(400, gT("This survey has not been activated."));
        }
        if (!Yii::getPathOfAlias('getQuestionInformation')) {
            throw new CHttpException(500, gT("Need getQuestionInformation plugin."));
        }
        $bAdminExportRight = Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export');
        $token = null;
        if ($oSurvey->anonymized != 'Y') {
            // Review , todo : add a restriction settings
            $token = Yii::app()->getRequest()->getParam('token');
            /* allow set tokenGroup according to settings */
        }
        $srid = Yii::app()->getRequest()->getParam('srid');
        $srids = Yii::app()->getRequest()->getParam('srids');

        $restrictedIds = null;
        $tokenPermission = null;
        if ($token || !$bAdminExportRight) {
            if ($token) {
                $tokenPermission = $this->get('tokenPermission', 'Survey', $surveyId, 'none');
                if (!self::extraPluginAllowTokenGroupPermission() && in_array($tokenPermission, ['restricted','manager','all'])) {
                    $tokenPermission = 'token';
                }
                switch ($tokenPermission) {
                    case 'all':
                    case 'token':
                        break;
                    case 'restricted':
                        $restrictedIds = \TokenUsersListAndManagePlugin\Utilities::getAllowedSridList($surveyId, $token);
                        break;
                    case 'manager':
                        if (!\TokenUsersListAndManagePlugin\Utilities::getIsManager($surveyId)) {
                            $tokenPermission = 'token';
                        }
                        break;
                    case 'none':
                    default:
                        $token = null;
                }
            }
            if ($srid && empty($srids)) {
                if (empty($_SESSION['survey_' . $surveyId]['srid']) || $_SESSION['survey_' . $surveyId]['srid'] != $srid || $this->get('currentSridPermission', 'Survey', $surveyId, 'Y') != 'Y') {
                    if ($tokenPermission == 'none') {
                        $srid = null;
                    }
                }
                if (!empty($srid)) {
                    $srids = [$srid];
                }
            }
        }
        if (!empty($srids) && $token) {
            if (!empty($restrictedIds)) {
                $srids = array_intersect($restrictedIds, $srids);
            } else {
                $criteria = new CDbCriteria();
                $criteria->select = ['id','token'];
                $criteria->addInCondition('id', $srids);
                if ($tokenPermission == 'token') {
                    $criteria->compare('token', $token);
                } else {
                    $criteria->addInCondition('token', \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token));
                }
                $restrictedIds = CHtml::listData(\Response::model($surveyId)->findAll($criteria), 'id', 'id');
                $srids = array_intersect($restrictedIds, $srids);
            }
            if (empty($srids)) {
                throw new CHttpException(404, $this->translate("Invalid response id."));
            }
        }
        if (!$bAdminExportRight && empty($token) && empty($srids)) {
            throw new CHttpException(401);
        }

        $language = Yii::app()->getRequest()->getParam(
            'exportlang',
            Yii::app()->getRequest()->getParam(
                'lang',
                $this->get('exportlang', 'Survey', $surveyId, null)
            )
        );
        if (!$language || !in_array($language, Survey::model()->findByPk($surveyId)->getAllLanguages())) {
            $language = Survey::model()->findByPk($surveyId)->language;
        }
        $oAvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($surveyId);
        $aChildrensSurveyExport = $oAvailableRelatedExport->getChildrenSurveyExports();
        $exportResponse = new \ExportResponseWithRelated\exports\Responses(
            $surveyId,
            $language
        );
        $exportResponse->disableIdCheck = boolval($this->get('disableIdCheck', 'Survey', $surveyId, 'N') == 'Y');
        $exportResponse->completed = boolval(App()->getRequest()->getParam('completed', $this->get('completed', 'Survey', $surveyId, false)));
        $exportResponse->answerFormat = strval(App()->getRequest()->getParam('answerCode', $this->get('answerCode', 'Survey', $surveyId, 'full')));
        $exportResponse->answerCode = ($exportResponse->answerFormat == 'code');
        $headerType = $exportResponse->dataHeader = strval(App()->getRequest()->getParam('answerHeader', $this->get('answerHeader', 'Survey', $surveyId, 'code')));
        if ($token) {
            $exportResponse->token = $token;
            $exportResponse->tokenGroup = true;
        }

        if ($srid) {
            $exportResponse->responseId = $srid;
        } elseif ($srids) {
            $exportResponse->responseIds = $srids;
        }
        /* Columns selection */
        $exportResponse->aColumns = $this->get('thisSurveyColumns', 'Survey', $surveyId);
        $exportResponse->aChildsColumns = $this->get('childSurveysColumns', 'Survey', $surveyId);

        foreach ($aChildrensSurveyExport as $relatedid => $availableExport) {
            $export = 'export';
            if (!$exportResponse->completed) {
                $export = 'exportall';
            }
            $exportResponse->addRelatedExport($relatedid, 'survey' . $relatedid, [$export]);
        }
        $adata = $exportResponse->export();
        $aRelatedDatas = $exportResponse->exportRelated();
        switch (App()->getRequest()->getParam('format', $this->get('format', 'Survey', $surveyId, 'xlsx'))) {
            case 'xlsx':
                $this->exportAsXlsx($surveyId, $adata, $aRelatedDatas, $headerType != 'none');
                break;
            case 'json':
            default:
                $this->exportAsJson($surveyId, $adata, $aRelatedDatas, $headerType != 'none');
        }
    }

    /**
     * export the data as a json array
     * @param integer $surveyId the survey id
     * @param array $aData the survey data
     * @param array[] $aRelatedDatas the related surveyus data
     * @param boolean $haveHeader
     * @return @void
     */
    private function exportAsJson($surveyId, $adata, $aRelatedDatas, $haveHeader)
    {
        Yii::import('application.helpers.viewHelper');
        \viewHelper::disableHtmlLogging();
        $jsonFlags = 0;
        if (App()->getRequest()->isPostRequest) {
            header('Content-disposition: attachment; filename=' . 'results-survey-childs-' . $surveyId . '.json');
            $jsonFlags = JSON_PRETTY_PRINT;
        }
        header('Content-Type: application/json');
        echo json_encode(
            [
            'survey' => $adata,
            'related' => $aRelatedDatas
            ],
            $jsonFlags
        );
        Yii::app()->end();
    }

    /**
     * export the data as a json array
     * @param integer $surveyId the survey id
     * @param array $aData the survey data
     * @param array[] $aRelatedDatas the related surveys data
     * @param boolean $haveHeader
     * @return @void
     */
    private function exportAsXlsx($surveyId, $aSurveyRows, $aRelatedExports, $haveHeader)
    {
        Yii::import('application.helpers.viewHelper');
        \viewHelper::disableHtmlLogging();
        $excelfilename = 'results-survey-children-' . $surveyId;
        if (trim($this->get('filename', 'Survey', $surveyId, ''))) {
            $excelfilename = trim($this->get('filename', 'Survey', $surveyId, ''));
        }
        $filename = App()->getRuntimePath() . DIRECTORY_SEPARATOR . App()->securityManager->generateRandomString(52);
        if (intval(App()->getConfig('versionnumber')) <= 3) {
            require_once(APPPATH . '/third_party/xlsx_writer/xlsxwriter.class.php');
        }
        /** Allow better sheet name */
        $currrentFancySheetName = App()->getRequest()->getParam('fancySheetName', $this->get('fancySheetName', 'Survey', $surveyId, 'N'));
        $fancySheetName = boolval($currrentFancySheetName) && !in_array($currrentFancySheetName, ['N','off','0']);

        $writer = new XLSXWriter();
        $writer->setTempDir(Yii::app()->getConfig('tempdir'));
        $currentSheet = "survey-" . $surveyId;
        if ($fancySheetName) {
            $currentSheet = $this->getFancySurveySheetname($surveyId);
            if (trim($this->get('sheetName', 'Survey', $surveyId, ''))) {
                $currentSheet = XLSXWriter::sanitize_sheetname(trim($this->get('sheetName', 'Survey', $surveyId, '')));
            }
        }
        $header = !$haveHeader;
        foreach ($aSurveyRows as $row) {
            if (!$header) {
                $writer->writeSheetRow($currentSheet, array_keys($row));
                $header = true;
            }
            $writer->writeSheetRow($currentSheet, $row);
        }
        $aChildsSheetNames = $this->get('sheetsName', 'Survey', $surveyId);
        foreach ($aRelatedExports as $survey => $aRelatedExport) {
            $childSurveyId = str_replace('survey', '', $survey);
            $currentSheet = $currentBaseSheet = "survey-" . $childSurveyId;
            foreach ($aRelatedExport as $export => $aRelatedSurveyRows) {
                $header = !$haveHeader;
                if (count($aRelatedExport) > 1) {
                    $currentSheet = $currentBaseSheet . "-" . $export;
                } elseif ($fancySheetName) {
                    $currentSheet = $this->getFancySurveySheetname(str_replace('survey', '', $survey));
                    if (!empty($aChildsSheetNames[$childSurveyId])) {
                        $currentSheet = XLSXWriter::sanitize_sheetname(trim($aChildsSheetNames[$childSurveyId]));
                    }
                }
                foreach ($aRelatedSurveyRows as $row) {
                    if (!$header) {
                        $writer->writeSheetRow($currentSheet, array_keys($row));
                        $header = true;
                    }
                    $writer->writeSheetRow($currentSheet, $row);
                }
            }
        }
        $writer->writeToFile($filename);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"{$excelfilename}.xlsx\"");
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        unlink($filename);
        Yii::app()->end();
    }

    /** Add the settings **/
    public function beforeSurveySettings()
    {
        $surveyId = $this->getEvent()->get('survey');
        $linkComplete = str_replace(
            array('SAVEDID','TOKEN'),
            array('{SAVEDID}','{TOKEN}'),
            App()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId))
        );
        $linkAll = str_replace(
            array('SAVEDID','TOKEN'),
            array('{SAVEDID}','{TOKEN}'),
            App()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId, 'completed' => '0'))
        );
        $linkWithSaveid = str_replace(
            array('SAVEDID','TOKEN'),
            array('{SAVEDID}','{TOKEN}'),
            App()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'srid' => 'SAVEDID'))
        );
        $linkWithToken = str_replace(
            array('SAVEDID','TOKEN'),
            array('{SAVEDID}','{TOKEN}'),
            App()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'token' => 'TOKEN'))
        );
        $content = "<ul>"
                . "<li><strong>All completed :</strong> <a href='{$linkComplete}'>{$linkComplete}</a></li>"
                . "<li><strong>All :</strong> <a href='{$linkAll}'>{$linkAll}</a></li>"
                . "<li><strong>All related to token :</strong> <a href='{$linkWithSaveid}'>{$linkWithSaveid}</a></li>"
                . "<li><strong>All related to responseid :</strong> <a href='{$linkWithToken}'>{$linkWithToken}</a></li>"
                . "</ul>";

        $this->getEvent()->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'active' => array(
                    'type' => 'info',
                    'content' => $content,
                ),
            )
        ));
    }

     /**
      * get translation
      * @param string $string to translate
      * @param string language, current by default
      * @return string
      */
    private function translate($string, $sLanguage = null)
    {
        return $this->gT($string, 'unescaped', $sLanguage);
    }

    /**
     * get the title of a survey by language avoiuding after find etx …
     * @param integer $surveyId
     * @param string $language : preferred language
     * @return string
     */
    private function getFancySurveySheetname($surveyId, $language = null)
    {
        $survey = Yii::app()->db->createCommand()
            ->select('language,additional_languages')
            ->from(Survey::model()->tableName())
            ->where('sid=:sid')
            ->bindParam(":sid", $surveyId, PDO::PARAM_INT)
            ->queryRow();

        $allLanguagues = explode(' ', $survey['additional_languages']);
        if (is_null($language) || !in_array($language, $allLanguagues)) {
            $language = $survey['language'];
        }
        $oSurveyLanguageSetting = SurveyLanguageSetting::model()->find([
            'select' => "surveyls_title",
            'condition' => "surveyls_survey_id = :sid AND surveyls_language = :language",
            'params' => [':sid' => $surveyId, ':language' => $language]
        ]);
        $sheetName = XLSXWriter::sanitize_sheetname($oSurveyLanguageSetting->surveyls_title);
        /* Add -$surveyId */
        $maxChar = 30 - strlen($surveyId);
        $sheetName = function_exists('mb_substr') ? mb_substr($sheetName, 0, $maxChar) : substr($sheetName, 0, $maxChar);
        $sheetName .= "-" . $surveyId;
        return $sheetName;
    }

    /**
     * check if Token allow group manager and other settings
     * @return boolean
     */
    private static function extraPluginAllowTokenGroupPermission()
    {
        return App()->getConfig('TokenUsersListAndManageAPI')
            && version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.35.0') >= 0
            && App()->getConfig('reloadAnyResponseApi');
    }

    /**
     * get basic data for view
     * @para integer $surveyId
     * @return array[]
     */
    private function getBasicData($surveyId)
    {
        $aData = array(
            'pluginClass' => get_class($this),
            'surveyId' => $surveyId,
        );
        $aData['lang'] = $this->getLanguageStrings();
        $aData['form'] = array(
            'close' => App()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId)),
        );
        if (intval(App()->getConfig('versionnumber')) < 4) {
            $aData['form']['close'] = App()->createUrl('admin/survey', array('sa' => 'view', 'surveyid' => $surveyId));
        }
        $aData['tabsUrl'] = array(
            'export' => App()->createUrl(
                'admin/pluginhelper',
                [
                    'sa' => 'sidebody',
                    'surveyid' => $surveyId,
                    'plugin' => get_class($this),
                    'method' => 'actionExport'
                ]
            ),
            'permission' => App()->createUrl(
                'admin/pluginhelper',
                [
                    'sa' => 'sidebody',
                    'surveyid' => $surveyId,
                    'plugin' => get_class($this),
                    'method' => 'actionPermission'
                ]
            ),
            'surveys' => App()->createUrl(
                'admin/pluginhelper',
                [
                    'sa' => 'sidebody',
                    'surveyid' => $surveyId,
                    'plugin' => get_class($this),
                    'method' => 'actionSurveysSettings'
                ]
            ),
        );
        return $aData;
    }
    /**
     * get language strings
     */
    private function getLanguageStrings()
    {
        return [
            'Export' => $this->gT('Export'),
            'Permission' => $this->gT('Permission'),
            'Columns selection' => $this->gT('Columns selection'),
        ];
    }

    /**
     * Get the question list data for settings
     * @param integer $surveyId
     * @param string $language
     * @return array[]
     */
    private function getQuestionListDataForSettings($surveyId)
    {
        /* The id and others (don't sdearch survey to avoid DB call)*/
        $aQuestionList = [
           'data' => [
               'id' => "[id] " . $this->translate("Response id"),
               'token' => "[token] " . $this->translate("Token"),
               'completed' => "[completed] " . $this->translate("Completed"),
               'lastpage' => "[lastpage] " . $this->translate("Last page"),
               'datestamp' => "[datestamp] " . $this->translate("Date stamp"),
               'seed' => "[seed] " . $this->translate("Seed"),

           ],
           'options' => [
               'id' => [
                   'data-html' => true,
                   'data-trigger' => 'hover focus',
                   'data-content' => $this->translate("Response id"),
                   'data-title' => $this->translate("Response id"),
                   'title' => $this->translate("Response id"),
               ],
               'token' => [
                   'data-html' => true,
                   'data-trigger' => 'hover focus',
                   'data-content' => $this->translate("Token"),
                   'data-title' => $this->translate("Token"),
                   'title' => $this->translate("Token"),
               ],
               'completed' => [
                   'data-html' => true,
                   'data-trigger' => 'hover focus',
                   'data-content' => $this->translate("Completed"),
                   'data-title' => $this->translate("Completed"),
                   'title' => $this->translate("Completed"),
               ],
               'lastpage' => [
                   'data-html' => true,
                   'data-trigger' => 'hover focus',
                   'data-content' => $this->translate("Last page"),
                   'data-title' => $this->translate("Last page"),
                   'title' => $this->translate("Last page"),
               ],
               'datestamp' => [
                   'data-html' => true,
                   'data-trigger' => 'hover focus',
                   'data-content' => $this->translate("Date stamp"),
                   'data-title' => $this->translate("Date stamp"),
                   'title' => $this->translate("Date stamp"),
               ],
               'seed' => [
                   'data-html' => true,
                   'data-trigger' => 'hover focus',
                   'data-content' => $this->translate("Seed"),
                   'data-title' => $this->translate("Seed"),
                   'title' => $this->translate("Seed"),
               ],
           ]
        ];
        if (tableExists('survey_' . $surveyId)) {
            $availableColumns = array_fill_keys(SurveyDynamic::model($surveyId)->getAttributes(), null);
            $aQuestionList['data'] = array_diff_key($aQuestionList['data'], $availableColumns);
            $aQuestionList['options'] = array_diff_key($aQuestionList['options'], $availableColumns);
        }
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $allQuestionListData = $surveyColumnsInformation->allQuestionListData();
        $aQuestionList['data'] = $aQuestionList['data'] + $allQuestionListData['data'];
        $aQuestionList['options'] = $aQuestionList['options'] + $allQuestionListData['options'];
        return $aQuestionList;
    }
}
