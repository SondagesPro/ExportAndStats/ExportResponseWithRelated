<div>
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?php if($activetab == 'export') { echo 'active'; } ?>"><a href="<?= $tabsUrl['export'] ?>"><?= $lang['Export'] ?></a></li>
    <li role="presentation" class="<?php if($activetab == 'permission') { echo 'active'; } ?>"><a href="<?= $tabsUrl['permission'] ?>"><?= $lang['Permission'] ?></a></li>
    <li role="presentation" class="<?php if($activetab == 'surveys') { echo 'active'; } ?>"><a href="<?= $tabsUrl['surveys'] ?>"><?= $lang['Columns selection'] ?></a></li>

  </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="Export">
            <?php $aData = get_defined_vars();
                $this->renderPartial('ExportResponseWithRelated.views.' . $settingview, $aData, false);
            ?>
        </div>
    </div>
</div>
