<?php echo CHtml::beginForm();?>
    <div class="clearfix h3 pagetitle"><?= $title ?>
      <div class='pull-right'>
        <?php
            echo CHtml::htmlButton(
                '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> ' . gT('Export'),
                array('type' => 'submit', 'name' => 'export' . $pluginClass, 'value' => 'export','class' => 'btn btn-primary')
            );
            echo " ";
            echo CHtml::link(gT('Close'), $form['close'], array('class' => 'btn btn-default'));
            ?>
      </div>
    </div>
    <div>
        <?php foreach ($aExportSettings as $legend => $settings) {
            $this->widget('ext.SettingsWidget.SettingsWidget', array(
                //'id'=>'summary',
                'title' => $legend,
                //'prefix' => 'responseListAndManage', // Inalid label before 3.13.3
                'form' => false,
                'formHtmlOptions' => array(
                    'class' => 'form-core',
                ),
                'labelWidth' => 6,
                'controlWidth' => 6,
                'settings' => $settings,
            ));
        } ?>
        <div class='row'>
          <div class='col-md-offset-6 submit-buttons'>
            <?php
                echo CHtml::htmlButton(
                    '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> ' . gT('Export'),
                    array('type' => 'submit', 'name' => 'export' . $pluginClass, 'value' => 'export','class' => 'btn btn-primary')
                );
                echo " ";
                    echo CHtml::htmlButton(
                        '<i class="fa fa-check" aria-hidden="true"></i> ' . gT('Save as default export'),
                        array('type' => 'submit', 'name' => 'saveasdefault' . $pluginClass, 'value' => 'saveasdefault','class' => 'btn btn-primary')
                    );
                    ?>
          </div>
        </div>
    </div>
<?php echo CHtml::endForm();?>
