<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2022 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.6.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace ExportResponseWithRelated;

class Utilities
{
    /** null|integer[] iPlugins */
    private static $iPlugins;
    /** null|array[] aPluginsSurveySettings */
    private static $pluginsSurveySettings;

    /**
     * Get a DB setting from a plugin
     * @param string $pluginName
     * @param integer $surveyId
     * @param string $sSetting
     * @param mixed $default if not set
     * @return mixed
     */
    public static function getSurveyPluginSetting($pluginName, $surveyId, $sSetting, $default = '')
    {

        if (!self::getPluginId($pluginName)) {
            return $default;
        }
        if (isset(self::$pluginsSurveySettings[$surveyId][$sSetting])) {
            return self::$pluginsSurveySettings[$surveyId][$sSetting];
        }
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . \App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => self::getPluginId($pluginName),
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (!empty($oSetting)) {
            self::$pluginsSurveySettings[$surveyId][$sSetting] = json_decode($oSetting->value);
        } else {
            self::$pluginsSurveySettings[$surveyId][$sSetting] = $default;
        }
        return self::$pluginsSurveySettings[$surveyId][$sSetting];
    }

    /**
     * get the id of a plugins
     * @param string $pluginName
     * @return integer (0 for not exist)
     */
    public static function getPluginId($pluginName)
    {
        if (isset(self::$iPlugins[$pluginName])) {
            return self::$iPlugins[$pluginName];
        }
        self::$iPlugins[$pluginName] = 0;
        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => $pluginName)
        );
        if ($oPlugin && $oPlugin->active) {
            self::$iPlugins[$pluginName] = $oPlugin->id;
        }
        return self::$iPlugins[$pluginName];
    }
    /**
     * Get a question attaribute
     * @param integer qid
     * @param string attribute
     * @param mixed default
     * @return mixed
     */
    public static function getQuestionAttribute($qid, $attribute, $default = null)
    {
        $oQuestionAttribute = \QuestionAttribute::model()->find(
            'qid = :qid AND attribute = :attribute',
            array(
                ':qid' => $qid,
                ':attribute' => $attribute,
            )
        );
        if (!empty($oQuestionAttribute)) {
            return $oQuestionAttribute->value;
        }
        return $default;
    }

    /**
     * Return a translated message
     * @param string $string
     * @return string
     */
    public static function translate($string, $sLanguage = null)
    {
        return \Yii::t(
            '',
            $string,
            array(),
            'ExportResponseWithRelatedMessages',
            $sLanguage
        );
    }

    /**
     * Check validity of a survey
     * @param integer $surveyId
     * @return : boolean
     */
    public static function isSurveyIsValid($surveyId)
    {
        $oSurvey = \Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            return false;
        }
        return true;
    }
}
